#pragma once

#include "level.hpp"

#include <array>
#include <cstdint>

namespace baba
{
constexpr auto MAX_LEVEL = 2U;

inline const std::array<Level, MAX_LEVEL> Level_List = {
	Level{ { Space{ Entity{ 0, {} } }, Space{}, Space{}, Space{} },
		{ Space{}, Space{}, Space{}, Space{} }, { Space{}, Space{}, Space{}, Space{} },
		{ Space{}, Space{}, Space{}, Space{} } },
	{ Level{ { Space{}, Space{ Entity{ 0, {} } }, Space{}, Space{} },
		{ Space{}, Space{}, Space{}, Space{} }, { Space{}, Space{}, Space{}, Space{} },
		{ Space{}, Space{}, Space{}, Space{} } } };
} // namespace baba
