#pragma once

#include <chrono>

namespace baba
{
enum class InputAction
{
	None,
	CursorUp,
	CursorDown,
	CursorLeft,
	CursorRight,
	MenuBtn,
	SelectBtn,
	BackBtn
};

InputAction GetInput(unsigned timeout = 10);
} // namespace baba
