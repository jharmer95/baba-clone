#pragma once

#include "attributes.hpp"
#include "interactions.hpp"
#include "layers.hpp"
#include "words.hpp"

#include <cstdint>
#include <initializer_list>
#include <string>
#include <unordered_set>

namespace baba
{
class Entity
{
public:
	~Entity() = default;
	Entity(Phrase phrase, std::unordered_set<Attribute> attributes);
	Entity(Phrase phrase, std::initializer_list<Attribute> attributes);
	Entity(const Entity& other) noexcept;
	Entity(Entity&&) noexcept;

	[[nodiscard]] Entity& operator=(const Entity& other) noexcept;
	Entity& operator=(Entity&&) = delete;

	[[nodiscard]] bool operator==(const Entity& other) noexcept;

	[[nodiscard]] std::string GetName() const noexcept;
	[[nodiscard]] Interaction GetInteraction(const Entity& other) const noexcept;

protected:
	Layer m_layer;
	Phrase m_phrase;
	std::unordered_set<Attribute> m_attributes;
};
}
