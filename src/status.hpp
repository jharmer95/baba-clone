#pragma once

#include <cstdint>

namespace baba
{
enum class GameStatus : uint_fast8_t
{
	Running,
	Paused,
	Quit,
	Error
};

enum class LevelStatus : uint_fast8_t
{
	Running,
	Win,
	Lose,
	Restart,
	Error
};
} // namespace baba
