#pragma once

#include "words.hpp"

#include <memory>
#include <sstream>
#include <string>
#include <vector>

namespace baba
{
class Level;

enum class RuleType
{
	None,
	Swap,
	Property,
	Lock
};

class IRule
{
public:
	virtual ~IRule() = default;
	IRule() = default;

	virtual std::unique_ptr<IRule> clone() const = 0;
	virtual void Apply() const noexcept = 0;
	[[nodiscard]] virtual std::string GetText() const noexcept = 0;
	virtual RuleType GetType() const noexcept { return RuleType::None; }
};

namespace rules
{
	class SwapRule : public IRule
	{
	public:
		SwapRule(Phrase ent1, Phrase ent2) : m_entity1(ent1), m_entity2(ent2) {}
		std::unique_ptr<IRule> clone() const override { return std::make_unique<SwapRule>(*this); }
		void Apply() const noexcept override
		{
			// TODO
		}

		std::string GetText() const noexcept override { return m_entity1.GetText() + " Is " + m_entity2.GetText(); }

		RuleType GetType() const noexcept override { return RuleType::Swap; }

	protected:
		Phrase m_entity1;
		Phrase m_entity2;
	};

	class PropertyRule : public IRule
	{
	public:
		PropertyRule(Phrase ent, Phrase attr) : m_entity(ent), m_attribute(attr) {}
		std::unique_ptr<IRule> clone() const override { return std::make_unique<PropertyRule>(*this); }
		void Apply() const noexcept override
		{
			// TODO
		}

		std::string GetText() const noexcept override { return m_entity.GetText() + " Is " + m_attribute.GetText(); }

		RuleType GetType() const noexcept override { return RuleType::Property; }

	protected:
		Phrase m_entity;
		Phrase m_attribute;
	};

	class LockRule : public IRule
	{
	public:
		LockRule(Phrase ent) : m_entity(ent) {}
		std::unique_ptr<IRule> clone() const override { return std::make_unique<LockRule>(*this); }
		void Apply() const noexcept override
		{
			// TODO
		}

		std::string GetText() const noexcept override { return m_entity.GetText() + " Is " + m_entity.GetText(); }

		RuleType GetType() const noexcept override { return RuleType::Lock; }

	protected:
		Phrase m_entity;
	};
} // namespace rules

class RuleSet
{
public:
	RuleSet(const Level* level) : m_level(level) {}
	void ApplyAll() const noexcept;
	void AddRules(std::vector<std::unique_ptr<IRule>> new_rules) noexcept;
	void ClearRules() noexcept;

protected:
	const Level* m_level;
	std::vector<std::unique_ptr<IRule>> m_rules{};
};
} // namespace baba
