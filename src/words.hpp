#pragma once

#include <set>
#include <string>
#include <string_view>

namespace baba
{
enum class Syntax
{
	None,
	Noun,
	Verb,
	Logical,
	Relational
};

class Phrase
{
public:
	Phrase() = delete;
	constexpr Phrase(std::string_view text, Syntax syntax) : m_text(text), m_syntax(syntax) {}

	constexpr bool operator==(const Phrase& other) const noexcept
	{
		return m_text == other.m_text && m_syntax == other.m_syntax;
	}

	[[nodiscard]] std::string GetText() const noexcept { return std::string(m_text); }
	[[nodiscard]] Syntax GetSyntax() const noexcept { return m_syntax; }

protected:
	std::string_view m_text;
	Syntax m_syntax;
};

namespace phrases
{
	inline constexpr Phrase Baba("Baba", Syntax::Noun);
	inline constexpr Phrase Is("Is", Syntax::Relational);
	inline constexpr Phrase You("You", Syntax::Verb);
	inline constexpr Phrase And("And", Syntax::Logical);
	inline constexpr Phrase Or("Or", Syntax::Logical);
	inline constexpr Phrase Rock("Rock", Syntax::Noun);
	inline constexpr Phrase Wall("Wall", Syntax::Noun);
	inline constexpr Phrase Flag("Flag", Syntax::Noun);
	inline constexpr Phrase Door("Door", Syntax::Noun);
	inline constexpr Phrase Key("Key", Syntax::Noun);
	inline constexpr Phrase Push("Push", Syntax::Verb);
	inline constexpr Phrase Shut("Shut", Syntax::Verb);
	inline constexpr Phrase Open("Open", Syntax::Verb);
	inline constexpr Phrase Float("Float", Syntax::Verb);
	inline constexpr Phrase Win("Win", Syntax::Verb);
	inline constexpr Phrase Sink("Sink", Syntax::Verb);
	inline constexpr Phrase Stop("Stop", Syntax::Verb);
	inline constexpr Phrase Defeat("Defeat", Syntax::Verb);
} // namespace phrases
} // namespace baba
