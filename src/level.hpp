#pragma once

#include "entity.hpp"
#include "rules.hpp"
#include "status.hpp"

#include <cstdint>
#include <initializer_list>
#include <memory>
#include <stdexcept>
#include <vector>

namespace baba
{
class Space
{
public:
	Space() = default;
	Space(const Entity&) = delete;
	Space(Entity&& entity) : m_entity(std::make_unique<Entity>(std::move(entity))) {}

	[[nodiscard]] bool HasEntity() const noexcept { return m_entity.get() != nullptr; }
	[[nodiscard]] Entity& GetEntity() noexcept { return *m_entity.get(); }
	[[nodiscard]] const Entity& GetEntity() const noexcept { return *m_entity.get(); }

protected:
	std::unique_ptr<Entity> m_entity;
};

class Level
{
public:
	Level(std::initializer_list<std::initializer_list<Space>> spaces) noexcept
		: m_status(LevelStatus::Running)
	{
		for (const auto& line : spaces)
		{
			std::vector<Space> tmp;

			for (const auto& space : line)
			{
				tmp.push_back(space);
			}

			m_grid.push_back(tmp);
		}
	}

	[[nodiscard]] Entity& GetEntity(uint_fast8_t x, uint_fast8_t y);
	[[nodiscard]] const Entity& GetEntity(uint_fast8_t x, uint_fast8_t y) const;
	[[nodiscard]] LevelStatus GetStatus() const noexcept { return m_status; }

protected:
	std::vector<std::vector<Space>> m_grid;
	LevelStatus m_status;
	RuleSet m_ruleset{ this };
};
} // namespace baba
