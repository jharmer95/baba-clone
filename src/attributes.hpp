#pragma once

#include <cstdint>

namespace baba
{
enum class Attribute : uint_fast8_t
{
	You,
	Sink,
	Defeat,
	Open,
	Shut,
	Hot,
	Melt,
	Push,
	Stop
};
} // namespace baba
