#pragma once

#include <cstdint>

namespace baba
{
enum class Interaction
{
	Nothing,
	DestroySelf,
	DestroyOther,
	Push,
	Stop,
	Win
};
} // namespace baba
