#pragma once

#include <cstdint>

namespace baba
{
enum class Layer : uint_fast8_t
{
	Normal,
	Background,
	Floating
};
} // namespace baba
