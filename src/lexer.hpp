#pragma once

#include "rules.hpp"
#include "words.hpp"

#include <algorithm>
#include <memory>
#include <optional>
#include <unordered_map>
#include <vector>

namespace baba
{
template<typename Container>
std::vector<std::unique_ptr<IRule>> CheckWordSequence(const Container& seq)
{
	Syntax cur_syntax;
	Syntax last_noun_verb = Syntax::None;
	Syntax last_syntax = Syntax::None;

	std::unordered_map<Syntax, bool> lex_table{ { Syntax::Noun, true }, { Syntax::Verb, false },
		{ Syntax::Logical, false }, { Syntax::Relational, false } };

	std::vector<Phrase> subjects{};
	std::vector<Phrase> verbs{};
	std::vector<Phrase> sources{};
	std::optional<Phrase> target{};

	std::vector<std::unique_ptr<IRule>> rules{};

	bool lex_fail = false;

	for (const auto& phrase : seq)
	{
		cur_syntax = phrase.GetSyntax();

		if (lex_fail || !lex_table[cur_syntax])
		{
			break;
		}

		switch (cur_syntax)
		{
			case Syntax::Noun:
				if (last_syntax == Syntax::Relational)
				{
					if (last_noun_verb == Syntax::Noun)
					{
						sources = std::move(subjects);
						subjects.clear();

						if (target)
						{
							lex_fail = true;
							break;
						}

						target = phrase;
					}
					else
					{
						lex_fail = true;
						break;
					}
				}

				if (std::find(subjects.begin(), subjects.end(), phrase) == subjects.end())
				{
					subjects.push_back(phrase);
				}

				lex_table[Syntax::Logical] = true;
				lex_table[Syntax::Relational] = true;
				lex_table[Syntax::Noun] = false;
				lex_table[Syntax::Verb] = false;
				last_noun_verb = Syntax::Noun;
				break;

			case Syntax::Verb:
				if (std::find(verbs.begin(), verbs.end(), phrase) == verbs.end())
				{
					verbs.push_back(phrase);
				}

				lex_table[Syntax::Logical] = true;
				lex_table[Syntax::Relational] = false;
				lex_table[Syntax::Noun] = false;
				lex_table[Syntax::Verb] = false;
				last_noun_verb = Syntax::Verb;
				break;

			case Syntax::Logical:
				lex_table[Syntax::Logical] = false;
				lex_table[Syntax::Relational] = false;
				lex_table[Syntax::Noun] = last_syntax == Syntax::Noun;
				lex_table[Syntax::Verb] = last_syntax == Syntax::Verb;
				break;

			case Syntax::Relational:
				lex_table[Syntax::Logical] = false;
				lex_table[Syntax::Relational] = false;
				lex_table[Syntax::Noun] = true;
				lex_table[Syntax::Verb] = true;
				break;

			default:
				lex_fail = true;
				break;
		}

		last_syntax = cur_syntax;
	}

	for (const auto& subject : subjects)
	{
		for (const auto& verb : verbs)
		{
			rules::PropertyRule rule(subject, verb);
			rules.push_back(rule.clone());
		}
	}

	for (const auto& source : sources)
	{
		if (target)
		{
			if (source == *target)
			{
				rules::LockRule rule(source);
				rules.push_back(rule.clone());
			}
			else
			{
				rules::SwapRule rule(source, *target);
				rules.push_back(rule.clone());
			}
		}
	}

	return rules;
}
} // namespace baba
