#include "lexer.hpp"

#ifndef USING_PCH
#	define CATCH_CONFIG_MAIN
#	include <catch2/catch.hpp>
#endif

using namespace baba;

TEST_CASE("Simple Property", "[simple, property]")
{
	const std::vector<Phrase> sequence{ phrases::Rock, phrases::Is, phrases::Push };
	const auto result = CheckWordSequence(sequence);

	REQUIRE(result.size() == 1);
	REQUIRE(result[0]->GetType() == RuleType::Property);
	REQUIRE_THAT(result[0]->GetText(), Catch::Matchers::Equals("Rock Is Push"));
}

TEST_CASE("Simple Swap", "[simple, swap]")
{
	const std::vector<Phrase> sequence{ phrases::Rock, phrases::Is, phrases::Door };
	const auto result = CheckWordSequence(sequence);

	REQUIRE(result.size() == 1);
	REQUIRE(result[0]->GetType() == RuleType::Swap);
	REQUIRE_THAT(result[0]->GetText(), Catch::Matchers::Equals("Rock Is Door"));
}

TEST_CASE("Simple Lock", "[simple, lock]")
{
	const std::vector<Phrase> sequence{ phrases::Rock, phrases::Is, phrases::Rock };
	const auto result = CheckWordSequence(sequence);

	REQUIRE(result.size() == 1);
	REQUIRE(result[0]->GetType() == RuleType::Lock);
	REQUIRE_THAT(result[0]->GetText(), Catch::Matchers::Equals("Rock Is Rock"));
}

TEST_CASE("Logical Property", "[simple, logical, property]")
{
	{
		const std::vector<Phrase> sequence{ phrases::Rock, phrases::And, phrases::Door, phrases::Is, phrases::Push };

		const auto result = CheckWordSequence(sequence);

		REQUIRE(result.size() == 2);

		for (const auto& rule : result)
		{
			REQUIRE(rule->GetType() == RuleType::Property);
		}

		REQUIRE_THAT(result[0]->GetText(), Catch::Matchers::Equals("Rock Is Push"));
		REQUIRE_THAT(result[1]->GetText(), Catch::Matchers::Equals("Door Is Push"));
	}
	{
		const std::vector<Phrase> sequence{ phrases::Rock, phrases::Is, phrases::Push, phrases::And, phrases::Float,
			phrases::And, phrases::Defeat };

		const auto result = CheckWordSequence(sequence);

		REQUIRE(result.size() == 3);

		for (const auto& rule : result)
		{
			REQUIRE(rule->GetType() == RuleType::Property);
		}

		REQUIRE_THAT(result[0]->GetText(), Catch::Matchers::Equals("Rock Is Push"));
		REQUIRE_THAT(result[1]->GetText(), Catch::Matchers::Equals("Rock Is Float"));
		REQUIRE_THAT(result[2]->GetText(), Catch::Matchers::Equals("Rock Is Defeat"));
	}
	{
		const std::vector<Phrase> sequence{ phrases::Rock, phrases::And, phrases::Door, phrases::And, phrases::Key,
			phrases::Is, phrases::Push, phrases::And, phrases::Float, phrases::And, phrases::Defeat };

		const auto result = CheckWordSequence(sequence);

		REQUIRE(result.size() == 9);

		for (const auto& rule : result)
		{
			REQUIRE(rule->GetType() == RuleType::Property);
		}

		REQUIRE_THAT(result[0]->GetText(), Catch::Matchers::Equals("Rock Is Push"));
		REQUIRE_THAT(result[1]->GetText(), Catch::Matchers::Equals("Rock Is Float"));
		REQUIRE_THAT(result[2]->GetText(), Catch::Matchers::Equals("Rock Is Defeat"));
		REQUIRE_THAT(result[3]->GetText(), Catch::Matchers::Equals("Door Is Push"));
		REQUIRE_THAT(result[4]->GetText(), Catch::Matchers::Equals("Door Is Float"));
		REQUIRE_THAT(result[5]->GetText(), Catch::Matchers::Equals("Door Is Defeat"));
		REQUIRE_THAT(result[6]->GetText(), Catch::Matchers::Equals("Key Is Push"));
		REQUIRE_THAT(result[7]->GetText(), Catch::Matchers::Equals("Key Is Float"));
		REQUIRE_THAT(result[8]->GetText(), Catch::Matchers::Equals("Key Is Defeat"));
	}
}

TEST_CASE("Logical Swap", "[simple, logical, swap]")
{
	{
		const std::vector<Phrase> sequence{ phrases::Rock, phrases::And, phrases::Door, phrases::Is, phrases::Key };
		const auto result = CheckWordSequence(sequence);

		REQUIRE(result.size() == 2);

		for (const auto& rule : result)
		{
			REQUIRE(rule->GetType() == RuleType::Swap);
		}

		REQUIRE_THAT(result[0]->GetText(), Catch::Matchers::Equals("Rock Is Key"));
		REQUIRE_THAT(result[1]->GetText(), Catch::Matchers::Equals("Door Is Key"));
	}
	{
		const std::vector<Phrase> sequence{ phrases::Rock, phrases::Is, phrases::Door, phrases::And, phrases::Flag,
			phrases::And, phrases::Key };

		const auto result = CheckWordSequence(sequence);

		REQUIRE(result.size() == 1);
	}
	{
		const std::vector<Phrase> sequence{ phrases::Rock, phrases::And, phrases::Door, phrases::And, phrases::Key,
			phrases::Is, phrases::Baba, phrases::And, phrases::Wall, phrases::And, phrases::Flag };

		const auto result = CheckWordSequence(sequence);

		REQUIRE(result.size() == 3);
	}
}

TEST_CASE("Mixed", "[simple, logical, swap, property, lock]")
{
	{
		const std::vector<Phrase> sequence{ phrases::Float, phrases::Is, phrases::Rock };

		const auto result = CheckWordSequence(sequence);
		REQUIRE(result.size() == 0);
	}
	{
		const std::vector<Phrase> sequence{ phrases::Float, phrases::And, phrases::Rock };

		const auto result = CheckWordSequence(sequence);
		REQUIRE(result.size() == 0);
	}
	{
		const std::vector<Phrase> sequence{ phrases::Rock, phrases::And, phrases::Rock };

		const auto result = CheckWordSequence(sequence);
		REQUIRE(result.size() == 0);
	}
	{
		const std::vector<Phrase> sequence{ phrases::Rock, phrases::And, phrases::Rock, phrases::Is, phrases::Float };

		const auto result = CheckWordSequence(sequence);
		REQUIRE(result.size() == 1);
	}
}
